import redis
import oprah
import logging
import base64
from base64 import b64encode
import json
import gzip
import time
from http.client import HTTPSConnection
from ssl import _create_unverified_context
import os
import threading

def checkhttpsproxy1(proxy):
	try:
		url = 'https://www.canadapost.ca/cpc/en/home.page'
		connection = HTTPSConnection(proxy['ip'] + ':' + str(proxy['port']),timeout=10,
	                                     context=_create_unverified_context())

		headers = None
		if 'user' in proxy:
			creds = ('%s:%s' % (proxy['user'], proxy['pass'])).encode('ascii')
			auth = base64.b64encode(creds).decode('ascii')
			headers = {'Proxy-Authorization': 'Basic %s' % auth}

		connection.request(method='GET', url=url,
	                           headers=headers)
		response = connection.getresponse()
		return {'status': response.status,
				'reason': response.reason}

	except Exception as e:
		logging.info('[Opera Proxy] Check Exception: %s' % e)
		return {'status': 1200}

def checkhttpsproxy2(proxy):
	try:
		url = 'https://www.amazon.com'
		connection = HTTPSConnection(proxy['ip'] + ':' + str(proxy['port']),timeout=10,
	                                     context=_create_unverified_context())

		headers = None
		if 'user' in proxy:
			creds = ('%s:%s' % (proxy['user'], proxy['pass'])).encode('ascii')
			auth = base64.b64encode(creds).decode('ascii')
			headers = {'Proxy-Authorization': 'Basic %s' % auth}

		connection.request(method='GET', url=url,
	                           headers=headers)
		response = connection.getresponse()
		return {'status': response.status,
				'reason': response.reason}

	except Exception as e:
		logging.info('[Opera Proxy] Check Exception: %s' % e)
		return {'status': 1200}

def checkhttpsproxy3(proxy):
	try:
		url = 'https://www.ups.com/track/api/Track/GetStatus?loc=en_US'
		connection = HTTPSConnection(proxy['ip'] + ':' + str(proxy['port']),timeout=10,
	                                     context=_create_unverified_context())

		headers = None
		id = '1Z12345E1512345676' #1Z12345E1512345676
		if 'user' in proxy:
			creds = ('%s:%s' % (proxy['user'], proxy['pass'])).encode('ascii')
			auth = base64.b64encode(creds).decode('ascii')
			headers = {'Proxy-Authorization': 'Basic %s' % auth, 
			'Referer':'https://www.ups.com/track?loc=en_US&tracknum=%s&requester=WT/trackdetails' % id,
             'X-Requested-With': 'XMLHttpRequest',
             "Content-type": "application/json"
             }
		params = json.dumps({ "Locale": "en_US", "TrackingNumber":[id]})
		connection.request(method='POST', url=url, body=params,
	                           headers=headers)
		response = connection.getresponse()
		obj = json.loads(response.read())
		if obj['statusCode'] == '200':
			return {'status': response.status,
				'reason': response.reason,
				'raw':obj}
		return {'status': 1200}

	except Exception as e:
		logging.info('[Opera Proxy] Check Exception: %s' % e)
		return {'status': 1200}

def checkhttpsproxy4(proxy):
	try:
		
		url = 'https://digitalapi.auspost.com.au/postcode/search?limit=10&q=3992'
		connection = HTTPSConnection(proxy['ip'] + ':' + str(proxy['port']),timeout=10,
	                                     context=_create_unverified_context())

		headers = None
		if 'user' in proxy:
			creds = ('%s:%s' % (proxy['user'], proxy['pass'])).encode('ascii')
			auth = base64.b64encode(creds).decode('ascii')
			headers = {'Proxy-Authorization': 'Basic %s' % auth,
			    'AUTH-KEY': '62b9613ddab3f8cdaf89c47c0234729e',
                'Accept': 'application/json',
			    'Accept-Encoding': 'gzip, deflate, br',
                'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/13.0 Safari/605.1.15',
                'Accept-Language': 'en-us'
             }
		connection.request(method='GET', url=url,
	                           headers=headers)
		response = connection.getresponse()
		gz = response.read()
		j = gzip.decompress(gz)
		obj = json.loads(j.decode('utf-8'))
		if obj['localities']['locality']:
			return {'status': response.status,
				'reason': response.reason,
				'raw':obj}
		return {'status': 1200}

	except Exception as e:
		logging.info('[Opera Proxy] Check Exception: %s' % e)
		return {'status': 1200}

def addToRedis(key,proxy):
	logging.info('Adding to Redis')
	#r.hset(key,proxy['ip'],json.dumps(proxy))

def getProxies():
	global proxies
	try:
		key = 'SILrMEPBmJuhomxWkfm3JalqHX2Eheg1YhlEZiMh8II'
		client = 'se0316'

		op = oprah.OprahProxy(client, key)
		op.register_subscriber()
		op.register_device()
		logging.info("[Opera Proxy] Device registered")

		# creds = ('%s:%s' % (op.device_id_hash, op.device_password)).encode('ascii')
		# auth = base64.b64encode(creds).decode('ascii')

		for country_code in op.geo_list():
			for item in op.discover(country_code):
				if 'result' in item:
					for ip in item['result']['data']['ips']:
						proxy = {'proto':'https','ip':ip['ip'],'port':ip['ports'][0],'geo':ip['geo']['country_code'],'user':op.device_id_hash,'pass':op.device_password}
						if op.device_id_hash:
							proxy['address'] = proxy['proto'] +  '://' +  proxy['user'] + ':' + proxy['pass'] + '@' +  proxy['ip'] + ':' + str(proxy['port'])
						else:
							proxy['address'] = proxy['proto'] +  '://' +  proxy['ip'] + ':' + str(proxy['port'])

						if any(x for x in proxies if x['ip'] == proxy['ip']):
							logging.debug('EXISTS IP: %s ', proxy['ip'])
						else:
							logging.debug('Adding IP: %s ', proxy['ip'])
							proxies.append(proxy)

	except Exception as e:
		logging.info('[Opera Proxy] Get Proxy Error: %s' % e)

def checkProxy(proxy):
	try:
		#result1 = checkhttpsproxy1(proxy)
		result2 = checkhttpsproxy2(proxy)
		#result3 = checkhttpsproxy3(proxy)
		result4 = checkhttpsproxy4(proxy)

		if result2['status'] == 200 and  result4['status'] == 200:
			addToRedis(redis_key,proxy)
			#r.expire(redis_key,86400) #expire the key after 24 hours
	except Exception as e:
		logging.info('[Opera Proxy] checkProxy Error: %s' % e)

def checkProxies():
	global proxies
	threads = []
	try:
		for proxy in proxies:
			#print(proxy)
			t = threading.Thread(target=checkProxy,args=(proxy,))
			threads.append(t)
			t.start()

	except Exception as e:
		logging.info('[Opera Proxy] Check Proxies Error: %s' % e)

logging.basicConfig(
	level=logging.INFO,
	format='%(asctime)s %(levelname)-8s %(message)s'
)

logging.info('[Opera Proxy] STARTED')
# host = os.getenv('REDISHOST', 'localhost')
# r = redis.StrictRedis(host=host, port=6379, db=0)
# logging.info("[Opera Proxy] Connected to Redis Instance")
proxies = []
timestamp = time.time()
redis_key = 'opera:'+ str(timestamp)

for i in range(6):
	try:
		getProxies()
	except Exception as e:
		logging.info('[Opera Proxy RANGE] Error: %s' % e)

checkProxies()