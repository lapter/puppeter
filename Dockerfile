FROM inetsoftware/alpine-tesseract:4.1.0 as builder
# ^ Container used to install tesseract from

FROM node:11-alpine

# Proper python STDOUT behaviour in docker
#ENV PYTHONUNBUFFERED 1

# Required for tesseract compilation and use
#ENV LC_ALL C
# add required tessdata
RUN mkdir -p /usr/share/tessdata
ADD https://github.com/tesseract-ocr/tessdata_best/raw/main/eng.traineddata /usr/share/tessdata/eng.traineddata

COPY --from=builder /tesseract/tesseract-git-4.* /tesseract/
# Install this version of tesseract
RUN set -x \
    && echo "http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories \
    && apk add --update --allow-untrusted /tesseract/tesseract-git-* \
    && rm  -rf /tesseract \
    && echo "done"
#RUN apk add --no-cache build-base python-dev py-pip jpeg-dev zlib-dev python3 python3-dev libxml2 libxml2-dev libxslt-dev
RUN apk add --no-cache build-base python-dev py-pip python3 python3-dev libxml2 libxml2-dev libxslt-dev

COPY requirements.txt .
COPY w8nc4.png .
RUN python3 -m pip install -r requirements.txt
RUN tesseract w8nc4.png output
RUN cat output.txt
COPY proxy_job.py .
COPY oprah.py .
RUN python3 proxy_job.py
RUN node --version
